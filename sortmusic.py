# |/usr/bin/env python3

"Sort a list of songs based on their number of plays."

import sys


def order_items(songs: list, i: int, gap: int) -> list:
    """
    Reorder the 'songs' list based on the 'i' and 'gap' parameters.

    Args:
        songs (list): A list of songs where each item is a tuple (song_name, number_of_plays).
        i (int): The starting index for reordering.
        gap (int): The gap or step size for comparing and moving elements.

    This function reorders the 'songs' list based on the 'i' and 'gap' values, following the Shell Sort algorithm.
    It compares and moves elements within the list to achieve descending order based on the number of plays.

    Returns:
        list: The reordered 'songs' list.
    """

    cancion = songs[i][0]
    visitas = songs[i][1]
    j = i
    while j >= gap and songs[j - gap][1] < visitas:
        songs[j] = songs[j - gap]
        j -= gap
        songs[j] = (cancion, visitas)
    return songs


def sort_music(songs: list) -> list:
    """Implement the Shell Sort algorithm to order the dictionary based on the number of plays.

    Args:
        dictionary (dict): A dictionary where keys are song names and values are the number of plays.

    Implement the Shell Sort algorithm to rearrange the songs in the dictionary. Order them in descending
    order based on the number of plays. You should consider songs with more plays as 'higher' in terms of
    popularity and place them towards the beginning of the dictionary.

    Your implementation should update the 'dictionary' in place, resulting in a sorted dictionary.
    """

    k = len(songs)
    gap = k // 2
    for place in range(gap, k):
        songs = order_items(songs, place, gap)
    gap //= 2

    return songs


def create_dictionary(arguments):
    """
    Create a dictionary from a list of arguments.

    Args:
        arguments (list): A list of elements where every pair represents a song name and its number of plays.

    This function takes a list of arguments and converts it into a dictionary where keys are song names, and values are
    the corresponding number of plays.

    Returns:
        dict: A dictionary with song names as keys and the number of plays as values.
    """

    songs: dict = {}
    numb = 0
    try:
        while numb < len(arguments):
            song_name = arguments[numb]
            numb += 1
            if numb < len(arguments):
                reproductions = int(arguments[numb])
                songs[song_name] = reproductions
            else:
                sys.exit(f"Error: '{arguments[numb]}' no es un número válido.")
            numb += 1
    except:
        sys.exit("Error: Se ha de proporcionar un número valido de reproducciones.")
    return songs


def main():
    args = sys.argv[1:]

    if len(args) % 2 != 0 or len(args) == 0:
        sys.exit("Error: Debes proporcionar pares de canción y número de reproducciones.")

    songs: dict = create_dictionary(args)

    sorted_songs: list = sort_music(list(songs.items()))

    sorted_dict: dict = dict(sorted_songs)
    print(sorted_dict)


if __name__ == '__main__':
    main()
